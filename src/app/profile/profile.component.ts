import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GetDataService } from '../get-data.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //Personal Info Variables
  person_id;
  person_record;
  name;
  email;
  phone;
  address;
  city;
  gender;
  //metric Varaibles
  insituteMetric;
  majorsMetric;
  tmMetric;
  omMetric;
  pMetric;
  //College Variables
  insituteCollege;
  majorsCollege;
  tmCollege;
  omCollege;
  pCollege;
  //uni Variables
  insituteUni;
  degree;
  cgpa;

  constructor(
    private _getData: GetDataService,
    private _activatedRouter: ActivatedRoute
  ) {
    this.person_id = this._activatedRouter.snapshot.paramMap.get('id');
    if (this.person_id) {
      this._getData.getOneData(this.person_id).subscribe(
        response => {
          this.setResponse(response);
        },
        err => {
          console.log(err);
        }
      );
    }
  }

  ngOnInit(): void {}

  setResponse(response) {
    this.person_record = response;
    this.name = response.firstname + ' ' + response.lastname;
    this.email = response.email;
    this.phone = response.phone;
    this.address = response.address;
    this.city = response.city;
    this.gender - response.gender;
    this.insituteMetric = response.metricInfo.insitute;
    this.majorsMetric = response.metricInfo.majors;
    this.tmMetric = response.metricInfo.totalMarks;
    this.omMetric = response.metricInfo.obtainMarks;
    this.pMetric = response.metricInfo.percentage;
    this.insituteCollege = response.collegeInfo.insitute;
    this.majorsCollege = response.collegeInfo.majors;
    this.tmCollege = response.collegeInfo.totalMarks;
    this.omCollege = response.collegeInfo.obtainMarks;
    this.pCollege = response.collegeInfo.percentage;
    this.insituteUni = response.uniInfo.insitute;
    this.degree = response.uniInfo.degree;
    this.cgpa = response.uniInfo.CGPA;
  }
}
