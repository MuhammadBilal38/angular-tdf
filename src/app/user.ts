export class User {
  constructor(
    public firstname: string,
    public lastname: string,
    public email: string,
    public phone: number,
    public address: string,
    public city: string,
    public gender: string,
    public m_institute: string,
    public m_majors: string,
    public m_totalMarks: number,
    public m_obtainMarks: number,
    public m_percentage: number,
    public c_institute: string,
    public c_majors: string,
    public c_totalMarks: number,
    public c_obtainMarks: number,
    public c_percentage: number,
    public u_institute: string,
    public u_degree: string,
    public u_cgpa: number
  ) {}
}
