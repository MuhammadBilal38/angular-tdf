import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  _url = 'http://localhost:3000/table';
  _urlOneData = 'http://localhost:3000/form/';
  constructor(private _http: HttpClient) {}

  getData() {
    return this._http.get<any>(this._url);
  }

  getOneData(personId) {
    console.log(this._http.get<any>(this._urlOneData + personId));
    return this._http.get<any>(this._urlOneData + personId);
  }
}
