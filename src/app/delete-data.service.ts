import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteDataService {
  _url = 'http://localhost:3000/delete/';
  constructor(private _http: HttpClient) {}

  delete(personId) {
    return this._http.delete(this._url + personId.toString());
  }
}
