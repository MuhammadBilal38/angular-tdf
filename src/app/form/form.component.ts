import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { RegisterService } from '../register.service';
import { ActivatedRoute } from '@angular/router';
import { GetDataService } from '../get-data.service';
import { UpdateDataService } from '../update-data.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  genders = ['Male', 'Female', 'Other'];
  userModule = new User(
    '',
    '',
    '',
    null,
    '',
    '',
    '',
    '',
    '',
    null,
    null,
    null,
    '',
    '',
    null,
    null,
    null,
    '',
    '',
    null
  );
  constructor(
    private _registerService: RegisterService,
    private _activatedRoute: ActivatedRoute,
    private _getData: GetDataService,
    private _upgateData: UpdateDataService
  ) {}

  ngOnInit(): void {}
  // if (this.edit_status) {
  //   this._upgateData
  //     .update(this.person_id, this.informationHolder.value)
  //     .subscribe(
  //       response => console.log('Updates'),
  //       error => console.log('error')
  //     );
  //   alert('the information is successfully updated');
  // } else {

  //     this._registerService.register(this.informationHolder.value).subscribe(
  //       response => console.log('sucess'),
  //       error => console.log('error')
  //     );
  //     alert('the Information is successfully added');
  //  // }
  //   this.informationHolder.reset();
  onSubmit() {
    console.log(this.userModule);
    // this._registerService.register(this.userModule).subscribe(
    //   response => console.log('sucess'),
    //   error => console.log('error')
    // );
    alert('the Information is successfully added');
  }
}
