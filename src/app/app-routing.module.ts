import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { TableComponent } from './table/table.component';
import { ProfileComponent } from './profile/profile.component';
import { CardlayoutComponent } from './cardlayout/cardlayout.component';
import { from } from 'rxjs';
const routes: Routes = [
  { path: '', component: FormComponent },
  { path: 'form', component: FormComponent },
  { path: 'table', component: TableComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'cardlayout', component: CardlayoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  FormComponent,
  TableComponent,
  ProfileComponent,
  CardlayoutComponent
];
