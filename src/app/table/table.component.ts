import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { HttpClient } from '@angular/common/http';

import { DeleteDataService } from '../delete-data.service';
import { Router } from '@angular/router';
import { from } from 'rxjs';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  recods = [];
  edit_status = false;

  constructor(
    private _getData: GetDataService,
    private _http: HttpClient,
    private _deleteService: DeleteDataService,
    private _router: Router
  ) {
    this._getData.getData().subscribe(
      response => console.log('sucess', (this.recods = response)),
      error => console.log('error', error)
    );
  }

  ngOnInit(): void {}
  onDelete(id, thisptr) {
    this._deleteService.delete(id).subscribe(
      response => console.log('successfuly Deleted'),
      err => console.log(err)
    );
    let td = thisptr.target.parentNode;
    let tr = td.parentNode;
    tr.parentNode.removeChild(tr);
  }

  onEdit(id) {
    this.edit_status = true;
    this._router.navigate(['/form', id, this.edit_status]);
  }
  onProfile(id) {
    this._router.navigate(['/profile', id]);
  }
}
