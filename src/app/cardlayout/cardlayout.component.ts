import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cardlayout',
  templateUrl: './cardlayout.component.html',
  styleUrls: ['./cardlayout.component.css']
})
export class CardlayoutComponent implements OnInit {
  recods = [];

  constructor(private _getData: GetDataService, private _router: Router) {
    this._getData.getData().subscribe(
      response => console.log('sucess', (this.recods = response)),
      error => console.log('error', error)
    );
  }

  ngOnInit(): void {}

  onProfileClick(id) {
    this._router.navigate(['/profile', id]);
  }
}
