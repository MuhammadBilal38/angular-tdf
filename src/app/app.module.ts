import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { from } from 'rxjs';
import { TableComponent } from './table/table.component';
import { ProfileComponent } from './profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { CardlayoutComponent } from './cardlayout/cardlayout.component';
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    FormComponent,
    TableComponent,
    ProfileComponent,
    CardlayoutComponent
  ],
  imports: [BrowserModule, FormsModule, HttpClientModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
